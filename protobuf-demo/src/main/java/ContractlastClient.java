/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import demo.TypedConsumer1;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.soap.SOAPBinding;

public final class ContractlastClient {

    private static final QName SERVICE_NAME
        = new QName("http://demo/", "TypedConsumer1");
    private static final QName PORT_NAME
        = new QName("http://demo/", "TypedConsumer1Port");


    private ContractlastClient() {
    }

    public static void main(String args[]) throws Exception {
        Service service = Service.create(SERVICE_NAME);
        // Endpoint Address
        String endpointAddress = "http://localhost:9998/app/contractlast";//"local://hello";
        // If web service deployed on Tomcat deployment, endpoint should be changed to:
        // http://localhost:8080/java_first_jaxws-<cxf-version>/services/hello_world

        // Add a port to the Service
        //service.addPort(PORT_NAME, SOAPBinding.SOAP11HTTP_BINDING, endpointAddress);
        service.addPort(PORT_NAME, SOAPBinding.SOAP12HTTP_MTOM_BINDING, endpointAddress);


        TypedConsumer1  hw = service.getPort(TypedConsumer1.class);
        System.out.println(hw.xxx("World"));


    }

}
