package demo

/**
 * Date: 04.07.12
 * Time: 17:09
 */

import javax.jws.WebService
import javax.ws.rs._

@WebService(endpointInterface = "demo.TypedConsumer1",serviceName = "HelloWorld")
@Path("/")
trait TypedConsumer1 {
  //@consume("file:data/input/foo")
  //def foo(body: String): Future[String]

  @GET
  @Path("/list2")
  @Consumes(value = Array("application/json"))
  def xxx(@QueryParam("text") yyy:String) : String

  //@consume("jetty:http://localhost:8877/camel/bar")
  //def bar(@Body body: String, @Header("X-Whatever") header: String): String
}


class TypedConsumer1Impl extends TypedConsumer1 {
  def endpointUri = "file:data/input/actor"

  //So we can create Promises


  /*def foo(body: String): Future[String] = {
    println("Received message: %s" format body)
    val ret = "R: %s" format body
    Promise.successful(ret)
  }*/
  //def bar(body: String, header: String) = "body=%s header=%s" format (body, header)


  def xxx(yyy: String) = {"xxxx"+yyy}

  {
    println("INSTANTIATED")
  }
}

