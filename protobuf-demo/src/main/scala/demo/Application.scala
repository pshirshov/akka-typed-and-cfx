package demo

import akka.actor.{TypedProps, TypedActor, Props, ActorSystem}
import org.springframework.context.support.{GenericApplicationContext, ClassPathXmlApplicationContext}
import org.apache.camel.CamelContext
import org.springframework.beans.factory.support.DefaultListableBeanFactory

/**
 * Date: 04.07.12
 * Time: 15:29
 */


object Application {

  var typedConsumerStatic: TypedConsumer1 = null;

  def getHwSvc():TypedConsumer1 = {
    typedConsumerStatic
  }

  def main(args: Array[String]) {
    val system = ActorSystem("MySystem")
    val greeter = system.actorOf(Props[GreetingActor], name = "greeter")

    val typedConsumer: TypedConsumer1 =
      TypedActor(system).typedActorOf(TypedProps[TypedConsumer1Impl]())
    typedConsumerStatic = typedConsumer

    //create parent BeanFactory
    /*val parentBeanFactory = new DefaultListableBeanFactory();
    //register your pre-fabricated object in it
    parentBeanFactory.registerSingleton("helloworldService", typedConsumer);
    //wrap BeanFactory inside ApplicationContext
    val parentContext =
      new GenericApplicationContext(parentBeanFactory);
    parentContext.refresh()*/

    val applicationContext = new ClassPathXmlApplicationContext("server-applicationContext.xml") //, parentContext)
    //val ctx: TypedConsumer1 = applicationContext.getBean("helloworldService").asInstanceOf[TypedConsumer1]




    greeter ! Greeting("Charlie Parker")
    val processor = new PartialFunction[String, Any] {
      def apply(v1: String) = println("Processed:" + v1)

      def isDefinedAt(x: String) = true
    }
    //println( "RRR: %s" format typedConsumer.foo("xxx").onSuccess(processor) )
    println( typedConsumer.xxx("zzzz") )

    val ln = readLine()
    system.shutdown()
  }
}