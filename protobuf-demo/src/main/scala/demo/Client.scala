package demo

import akka.actor.ActorSystem

/**
 * Date: 04.07.12
 * Time: 15:36
 */

object Client {
  def main(args: Array[String]) {

    import com.typesafe.config.ConfigFactory
    val customConf = ConfigFactory.parseString(
"""
akka {
    loglevel = DEBUG

    actor {
        provider = "akka.remote.RemoteActorRefProvider"
    }

    remote {
        transport = "akka.remote.netty.NettyRemoteTransport"
        log-sent-messages = on
        log-received-messages = on
    }
}
""")

    val system = ActorSystem("MySystem", ConfigFactory.load(customConf))
    val greeter = system.actorFor("akka://MySystem@localhost:2552/user/greeter")

    greeter ! Greeting("Sonny Rollins")
    system.shutdown()
  }
}