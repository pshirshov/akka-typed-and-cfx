package demo

import akka.actor.{Actor, ActorLogging}

/**
 * Date: 04.07.12
 * Time: 15:13
 */

case class Greeting(who: String)// extends Serializable

class GreetingActor extends Actor with ActorLogging {
  def receive = {
    case Greeting(who) ⇒ log.error(who)
  }

}


